This is just a minimally updated version of [owsky's Itch-Bundle-Claimer](https://github.com/owsky/Itch-Bundle-Claimer), so that it works for the new version of [greasemonkey](https://www.greasespot.net/), this has been tested on Firefox, Chome, and Edge

## Installation
Once you have installed greasemonkey, simply click the "new user script" button, and paste the contents of ```script.js``` into the text field. Then disable greasemonkey and navigate to the bundle page you want to claim (should be in the bundles tab under "My Library/My Purchases"), then reactivate greasemonkey and refresh the page.

You can tab away and open new tabls on top of the script, and it will navigate through the pages on its own.


### Note
Please do not abuse this script, the itch team have made it clear that scripts like these are not kind to their servers. This only exists because it is a more efficient way to do it than similar scripts found online.

I recommend just adding games that look interesting to your library and checking back every now again, but I'm weird about having things in as few places as possible, so I still use this.
